1.First, open the include directory, which by default is located here: 
"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\include". 
Left-click the file glut.h and drag it into the include folder.


2.Next, open the lib directory, which by default is located here: 
"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\lib". 
Left-click the file glut32.lib and drag it into the lib folder.


3.Finally, open the system directory, which by default is located here: 
"C:\Windows\system". Left-click the file glut32.dll and drag it into the system folder.