#ifndef RUBIK_H
# define RUBIK_H

#include <iostream>
using namespace std;

struct stek
{
	char ch[2];
	int count;
	stek() {
		count=0;
		ch[0]=ch[1]='\0';
	}

	void add(char a, char b) {
		if(count==0) {
			ch[count]=a; count++;
			ch[count]=b; count++;
		}
		else if(ch[count-1]==a) ch[count-1]=b;
	}

	void show() {
		// if(ch[0]!=ch[1]) cout<<ch[0]<<"->"<<ch[1]<<' ';
		ch[0]=ch[1]='\0';
		count=0;
	}
};

void init();
void draw();
void navigate(unsigned char key,int x,int y);

// char green[3][3];
// char red[3][3];
// char orange[3][3];
// char blue[3][3];
// char yellow[3][3];
// char dark[3][3];
// int  err=1;

void drawBlue();
void drawRed();
void drawOrange();
void drawYellow();
void drawGreen();
void drawDark();

void colorize(char col[3][3],int i,int j);

void right();	void _right();
void left();	void _left();
void front();	void _front();
void up();		void _up();
void bot();		void _bot();
void axis_u();	void _axis_u();
void mid_u();

void menu();
void input();
void standard();
void shuffle();
void bugged_on_3();

void _1_cross();
void _2_1floor();
void _3_2floor();
void _4_top_cross();
void _5_3floor_edges();
void _6_3floor_corners();
void _7_final();

#endif