#include "rubik.h"
#include <GL/glut.h>

extern char green[3][3];
extern char red[3][3];
extern char orange[3][3];
extern char blue[3][3];
extern char yellow[3][3];
extern char dark[3][3];

void drawDark() {
	float x1,x2,x3,x4,y1,y2,y3,y4,z=0.0;
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			colorize(dark,i,j);

			x1=272.0/1064.0+j*(88.0/1064.0);
			x2=351.0/1064.0+j*(88.0/1064.0);
			x3=x2;x4=x1;

			y1=792.0/800.0-i*(88.0/800.0);
			y2=y1;
			y3=713.0/800.0-i*(88.0/800.0);
			y4=y3;		
			glBegin(GL_QUADS);
				glVertex3f(x1,y1,z);
				glVertex3f(x2,y2,z);
				glVertex3f(x3,y3,z);
				glVertex3f(x4,y4,z);
			glEnd();
		}
	}
}
void drawGreen() {
	float x1,x2,x3,x4,y1,y2,y3,y4,z=0.0;
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {

			colorize(green,i,j);
			x1=272.0/1064.0+j*(88.0/1064.0);
			x2=351.0/1064.0+j*(88.0/1064.0);
			x3=x2;x4=x1;

			y1=528.0/800.0-i*(88.0/800.0);
			y2=y1;
			y3=447.0/800.0-i*(88.0/800.0);
			y4=y3;		
			glBegin(GL_QUADS);
				glVertex3f(x1,y1,z);
				glVertex3f(x2,y2,z);
				glVertex3f(x3,y3,z);
				glVertex3f(x4,y4,z);
			glEnd();
		}
	}
}
void drawYellow() {
	float x1,x2,x3,x4,y1,y2,y3,y4,z=0.0;
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			colorize(yellow,i,j);
			x1=272.0/1064.0+j*(88.0/1064.0);
			x2=351.0/1064.0+j*(88.0/1064.0);
			x3=x2;x4=x1;

			y1=264.0/800.0-i*(88.0/800.0);
			y2=y1;
			y3=185.0/800.0-i*(88.0/800.0);
			y4=y3;		
			glBegin(GL_QUADS);
				glVertex3f(x1,y1,z);
				glVertex3f(x2,y2,z);
				glVertex3f(x3,y3,z);
				glVertex3f(x4,y4,z);
			glEnd();
		}
	}
}
void drawOrange() {
	float x1,x2,x3,x4,y1,y2,y3,y4,z=0.0;
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			colorize(orange,i,j);
			x1=8.0/1064.0+j*(88.0/1064.0);
			x2=87.0/1064.0+j*(88.0/1064.0);
			x3=x2;x4=x1;

			y1=528.0/800.0-i*(88.0/800.0);
			y2=y1;
			y3=447.0/800.0-i*(88.0/800.0);
			y4=y3;		
			glBegin(GL_QUADS);
				glVertex3f(x1,y1,z);
				glVertex3f(x2,y2,z);
				glVertex3f(x3,y3,z);
				glVertex3f(x4,y4,z);
			glEnd();
		}
	}
}
void drawRed() {
	float x1,x2,x3,x4,y1,y2,y3,y4,z=0.0;
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			colorize(red,i,j);
			x1=536.0/1064.0+j*(88.0/1064.0);
			x2=615.0/1064.0+j*(88.0/1064.0);
			x3=x2;x4=x1;

			y1=528.0/800.0-i*(88.0/800.0);
			y2=y1;
			y3=447.0/800.0-i*(88.0/800.0);
			y4=y3;		
			glBegin(GL_QUADS);
				glVertex3f(x1,y1,z);
				glVertex3f(x2,y2,z);
				glVertex3f(x3,y3,z);
				glVertex3f(x4,y4,z);
			glEnd();
		}
	}
}
void drawBlue() {
	float x1,x2,x3,x4,y1,y2,y3,y4,z=0.0;
	int i,j;
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {
			colorize(blue,i,j);
			x1=800.0/1064.0+j*(88.0/1064.0);
			x2=879.0/1064.0+j*(88.0/1064.0);
			x3=x2;x4=x1;

			y1=528.0/800.0-i*(88.0/800.0);
			y2=y1;
			y3=447.0/800.0-i*(88.0/800.0);
			y4=y3;		
			glBegin(GL_QUADS);
				glVertex3f(x1,y1,z);
				glVertex3f(x2,y2,z);
				glVertex3f(x3,y3,z);
				glVertex3f(x4,y4,z);
			glEnd();
		}
	}
}