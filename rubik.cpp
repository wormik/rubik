#include <cstdlib>
#include <iostream>
#include <GL/glut.h>
// #include "stek.cpp"
#include "rubik.h"
#include <vector>
using namespace std;

char green[3][3];
char red[3][3];
char orange[3][3];
char blue[3][3];
char yellow[3][3];
char dark[3][3];
int  err=1;
stek obj = {};
vector<char> spins;

void input_all()
{
	cout << "Input all:";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>green[i][j];
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>red[i][j];
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>orange[i][j];
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>blue[i][j];
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>yellow[i][j];
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>dark[i][j];	
}

int main(int argc, char *argv[])
{
	system("cls");
	char ch;
	cout<<"+-----------------------+\n"
		<<"+ Welcome into Q.U.B.E. |\n"
		<<"+-----------------------+\n";
	srand (time(NULL));

	
	cout<<endl
		<<"I: input combination\n"
		<<"S: set standard cube\n"
		<<"Your choise: ";
	cin>>ch;
	menu();
	if(ch=='s')standard();
	if(ch=='i')input();
	if(ch=='b')input_all();
	err--;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(100, 60);
	glutInitWindowPosition(200,0);
	glutCreateWindow("R.U.B.E.");
	init();


	glutDisplayFunc(draw);
	glutKeyboardFunc(navigate);
	
	glutMainLoop();

	return 0;
}

void input()
{
	cout<<"Input Green : ";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>green[i][j];
	cout<<"Input Red   : ";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>red[i][j];
	cout<<"Input Orange: ";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>orange[i][j];
	cout<<"Input Blue  : ";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>blue[i][j];
	cout<<"Input Yellow: ";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>yellow[i][j];
	cout<<"Input Dark  : ";
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
					cin>>dark[i][j];	
}

void menu()
{
	cout<<endl
		<<"=======Controls=======##\n"
		<<"##   D: Shuffle       ##\n"
		<<"##   A: Set Standard  ##\n"
		<<"## 1-7: Use algorithm ##\n"
		<<"##   0: Exit          ##\n"
		<<"##   C: Clear screen  ##\n"
		<<"##   M: Show menu     ##\n"
		<<"##   S: Solve         ##\n"
		<<"========================\n";
}