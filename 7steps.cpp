#include "rubik.h"
#include <vector>

extern char green[3][3];
extern char red[3][3];
extern char orange[3][3];
extern char blue[3][3];
extern char yellow[3][3];
extern char dark[3][3];


void _1_cross()
{
	int k,i,j;
	//Main cycle
	for(;;) {
		if(yellow[0][1]==yellow[1][1] && yellow[1][2]==yellow[1][1] && 
		   yellow[2][1]==yellow[1][1] && yellow[1][0]==yellow[1][1]) {
		 //   	refactor_spins();
			// print_spins();
			return;
		}

		//bot side 
		for(j=0;j<4;j++) {
			if(yellow[0][1]==yellow[1][1] && green[2][1]!=green[1][1]) {
				k=0;

				if(green[2][1]==orange[1][1])	k=1;
				if(green[2][1]==blue[1][1])		k=2;
				if(green[2][1]==red[1][1])		k=3;
				
				front();
				for(i=0;i<k;i++) bot();
				_front();
				for(;k;k--) _bot();
			}
			axis_u();
		}

		//1 floor
		for(j=0;j<4;j++) {
			k=0;
			if(green[2][1]==yellow[1][1]) {
				front();front();

				for(;dark[2][1]!=green[1][1];) {
					axis_u();k++;_up();
				}
				_front();
				_bot();
				left();
				bot();
				
				for(;k;k--) _axis_u();
			}
			axis_u();
		}		

		//2 floor left
		for(j=0;j<4;j++) {
			k=0;
			if(green[1][0]==yellow[1][1]) {

				if(orange[1][2]==orange[1][1])	k=0;
				if(orange[1][2]==blue[1][1])	k=1;
				if(orange[1][2]==red[1][1])		k=2;
				if(orange[1][2]==green[1][1])	k=3;
			
				for(i=0;i<k;i++) bot();
				left();
				for(;k;k--) _bot();
			}
			axis_u();
		}

		//2 floor right
		for(j=0;j<4;j++) {
			k=0;
			if(green[1][2]==yellow[1][1]) {

				if(red[1][0]==red[1][1])	k=0;
				if(red[1][0]==green[1][1])	k=1;
				if(red[1][0]==orange[1][1])	k=2;
				if(red[1][0]==blue [1][1])	k=3;
				
				for(i=0;i<k;i++) bot();
				_right();
				for(;k;k--) _bot();
			}
			axis_u();
		}

		k=0;
		//3 floor
		for(j=0;j<4;j++) {
			if(green[0][1]==yellow[1][1]) {
				for(;dark[2][1]!=green[1][1];) {
					axis_u();k++;_up();
				}
				_front();
				_bot();
				left();
				bot();
				for(;k;k--) _axis_u();
			}
			axis_u();
		}

		//top side
		for(j=0;j<4;j++) {
			k=0;
			if(dark[1][0]==yellow[1][1]) {
				if(orange[0][1]==orange[1][1])	k=0;
				if(orange[0][1]==blue [1][1])	k=1;
				if(orange[0][1]==red[1][1])		k=2;
				if(orange[0][1]==green[1][1])	k=3;
			
				for(i=0;i<k;i++) bot();
				left();left();
				for(;k;k--) _bot();
			}
			up();
		}
	}
}

void _2_1floor()
{
	int k,i,j;
	//main cycle
	for(;;) {
		if(yellow[0][0]==yellow[1][1] && green[2][0]==green[1][1]   && orange[2][2]==orange[1][1] &&
		   yellow[0][2]==yellow[1][1] && green[2][2]==green[1][1]   && red[2][0]==red[1][1] &&
		   yellow[2][2]==yellow[1][1] && red[2][2]==red[1][1]	    && blue[2][0]==blue[1][1] &&
		   yellow[2][0]==yellow[1][1] && orange[2][0]==orange[1][1] && blue[2][2]==blue[1][1]) break;

		//3 floor left
		for(j=0;j<4;j++) {
			if(green[0][0]==yellow[1][1]) {
				k=0;
				for(;dark[2][0]!=green[1][1];) {
					axis_u();k++;_up();
				}
				left();
				_front();
				_left();
				front();
				for(;k;k--) _axis_u();
			}
			axis_u();
		}

		//3 floor right
		for(j=0;j<4;j++) {
			if(green[0][2]==yellow[1][1]) {
				k=0;
				for(;dark[2][2]!=green[1][1];) {
					axis_u();k++;_up();
				}
				_right();
				front();
				right();
				_front();
				for(;k;k--) _axis_u();
			}
			axis_u();
		}

		//top side
		for(j=0;j<4;j++) {
			if(dark[2][0]==yellow[1][1]) {
				k=0;
				for(;orange[0][2]!=green[1][1];) {
					axis_u();k++;_up();
				}
				for(i=0;i<3;i++) {
					left();
					_front();
					_left();
					front();	
				}
				for(;k;k--) _axis_u();
			}
			axis_u();
		}

		//corners
		k=0;
		for(j=0;j<4;j++) {
			if(yellow[0][0]==yellow[1][1] && green[2][0]!=green[1][1]) {
				_left();
				up();
				left();
				break;
			}
			axis_u();k++;
		}
		for(;k;k--) _axis_u();

		//1 floor left
		k=0;
		for(j=0;j<4;j++) {
			if (green[2][0]==yellow[1][1] || orange[2][2]==yellow[1][1]) {
				left();
				_front();
				_left();
				front();
				break;
			}
			axis_u();k++;
		}
		for(;k;k--) _axis_u();
		axis_u();
	}
	while(green[1][1]!='g')axis_u();
}

void _3_2floor() 
{
	int j,k;
	//main cycle
	for(int f = 0;f < 50; ++f) {
		if(green[1][0]==green[1][1]   && green[1][2]==green[1][1] &&
		   red[1][0]==red[1][1] 	  && red[1][2]==red[1][1] &&
		   orange[1][0]==orange[1][1] && orange[1][2]==orange[1][1] &&
		   blue[1][0]==blue[1][1]	  && blue[1][2]==blue[1][1]) break;

		//replacing v---[]---v
		//			[]		[]
		for(j=0;j<4;j++) {
			if(green[0][1]!=dark[1][1] && dark[2][1]!=dark[1][1]) {
				k=0;
				for(;green[0][1]!=green[1][1];) {
					axis_u();k++;_up();
				}
				if(dark[2][1]==orange[1][1]) {
					_up();
					_left();
					up();
					left();
					up();
					front();
					_up();
					_front();
				}
				if(dark[2][1]==red[1][1]) {
					up();
					right();
					_up();
					_right();
					_up();
					_front();
					up();
					front();
				}
			}
			axis_u();
		}

		//troubleshot
		k=4;
		for(j=0;j<4;j++) {
			if (green[1][2]!=green[1][1] && red[1][0]!=red[1][1] &&
				green[1][2]!=dark[1][1]	 && red[1][0]!=dark[1][1]) {
				right();
				_up();
				_right();
				_up();
				_front();
				up();
				front();
				for(;k;k--)axis_u();break;
			}

			if(green[1][0]!=green[1][1] && orange[1][2]!=orange[1][1] &&
			   green[1][0]!=dark[1][1]	&& orange[1][2]!=dark[1][1]) {
				_left();
				up();
				left();
				up();
				front();
				_up();
				_front();
				for(;k;k--)axis_u();break;
			}

			if (green[1][2]==green[1][1] && red[1][0]!=red[1][1] &&
				red[1][0]!=dark[1][1]) {
				right();
				_up();
				_right();
				_up();
				_front();
				up();
				front();
				for(;k;k--)axis_u();break;
			}

			axis_u();k--;
		}
	}
	while(green[1][1]!='g')axis_u();
}

void _4_top_cross()
{
	int j;
	//main cycle
	for(;;) {
		if(dark[0][1]==dark[1][1] && dark[1][2]==dark[1][1] && 
		   dark[1][0]==dark[1][1] && dark[2][1]==dark[1][1]) break;
		for(j=0;j<4;j++) {
			if(dark[2][1]!=dark[1][1] && dark[1][2]!=dark[1][1]) {
				front();
				up();
				right();
				_up();
				_right();
				_front();
			}
			if(dark[2][1]!=dark[1][1] && dark[0][1]!=dark[1][1]) {
				front();
				right();
				up();
				_right();
				_up();
				_front();
			}
			axis_u();
		}
	}
	while(green[1][1]!='g')axis_u();
}

void _5_3floor_edges()
{
	int j,i;
	bool can=true,other=false;
	//main cycle
	for(;;) {
		if(green[0][1]==green[1][1]   && red[0][1]==red[1][1] &&
		   orange[0][1]==orange[1][1] && blue[0][1]==blue[1][1]) break;

		for(i=0;i<4 && can;i++) {
			for(j=0;j<4;j++) {
				if(green[0][1]==orange[1][1] && orange[0][1]==green[1][1] && can) {
					right();
					up();
					_right();
					up();
					right();
					up();up();
					_right();
					up();
					can=false;
					other=true;
				}
				if(can)up();
			}
			if(can)axis_u();
		}
		if(can) {
			right();
			up();
			_right();
			up();
			right();
			up();up();
			_right();
			up();
		}

		if(other) {
			axis_u();
			axis_u();
			if(green[0][1]==orange[1][1] && orange[0][1]==green[1][1]) {
				right();
				up();
				_right();
				up();
				right();
				up();up();
				_right();
				up();
			}
		}
	}
	while(green[1][1]!='g')axis_u();
}

void _6_3floor_corners()
{
	int j;
	bool one=false;
	//main cycle
	for(;;) {
		for(j=0;j<4 && !one;j++) {
			if(dark[2][2]==dark[1][1]   || dark[2][2]==green[1][1]  || dark[2][2]==red[1][1] )
			if(green[0][2]==dark[1][1]  || green[0][2]==green[1][1] || green[0][2]==red[1][1])
			if(red[0][0]==dark[1][1]	|| red[0][0]==green[1][1]	|| red[0][0]==red[1][1]  ) {
					one=true;
					break;
			}
			axis_u();
		}

		if(!one) {
			_right();
			_bot();_bot();
			right();
			_up();
			_right();
			_bot();_bot();
			right();
			_up();
			_right();
			_bot();_bot();
			right();
			up();up();
			_right();
			_bot();
			_bot();
			right();
		}

		if(one) {
			_axis_u();
			for(;;) {
				if( (dark[2][2]==dark[1][1]   || dark[2][2]==green[1][1]  || dark[2][2]==red[1][1] ) &&
					(green[0][2]==dark[1][1]  || green[0][2]==green[1][1] || green[0][2]==red[1][1]) &&
					(red[0][0]==dark[1][1]	  || red[0][0]==green[1][1]	  || red[0][0]==red[1][1]) ) return;
				else {
					_right();
					_bot();_bot();
					right();
					_up();
					_right();
					_bot();_bot();
					right();
					_up();
					_right();
					_bot();_bot();
					right();
					up();up();
					_right();
					_bot();
					_bot();
					right();
				}
			}
		}

	}
}

void _7_final()
{
	int j;
	for(j=0;j<4;j++) {
		if(green[0][2]==dark[1][1]) {
			right();
			_front();
			_right();
			front();
			right();
			_front();
			_right();
			front();
		}
		if(red[0][0]==dark[1][1]) {
			_front();
			right();
			front();
			_right();
			_front();
			right();
			front();
			_right();
		}
		up();
	}
	while(green[1][1]!='g')axis_u();
}