#include <GL/glut.h>
#include "rubik.h"
#include <iostream>
#include <vector>
#include <string>
#include <cctype>
using namespace std;

extern char green[3][3];
extern char red[3][3];
extern char orange[3][3];
extern char blue[3][3];
extern char yellow[3][3];
extern char dark[3][3];
extern int  err;
extern stek obj;
extern vector<char> spins;

bool pass=false,_pass=false,bpass=false;

void shuffle()
{
	int r;
	for(int i = 0; i < 10; i++) {
		r = 0;
		r = rand() % 5;
		switch(r) {
		case 0:
			front();
			break;
		case 1:
			right();
			break;
		case 2:
			left();
			break;
		case 3:
			up();
			break;
		case 4:
			bot();
			break;
		}
	}
	spins.clear();
	glutPostRedisplay();
}

void standard()
{
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
				green[i][j]= 'g';
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
				red[i][j]=   'r';
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
				orange[i][j]='o';
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
				blue[i][j]=  'b';
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
				yellow[i][j]='y';
	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
				dark[i][j]=  'd';	
	if(!err) glutPostRedisplay();
}

void bugged_on_3()
{
}

void push_spin(char side, bool backward)
{
	int shift = 0;
	switch (green[1][1]) {
	case 'g':
		shift = 0;
		break;
	case 'r':
		shift = 1;
		break;
	case 'b':
		shift = 2;
		break;
	case 'o':
		shift = 3;
		break;
	default:
		cout << "error";
		exit(0);
	}

	if (side == 'D' || side == 'U')
		shift = 0;

	string drum = "FRBLFRBDU";
	auto pos = drum.find(side);
	// cout << "POS" << pos << "SIDE" << side;
	auto c = drum[pos + shift];
	cout << c;
	if (backward)
		cout << "' ";
	else 
		cout << " ";
	if (backward) 
		c = tolower(c);
	spins.push_back(c);
}


void right()
{
	int i;
	int t=red[0][0];
	int t2=red[1][0];
	red[0][0]=red[2][0];
	red[1][0]=red[2][1];
	red[2][0]=red[2][2];
	red[2][1]=red[1][2];
	red[2][2]=red[0][2];
	red[1][2]=red[0][1];
	red[0][2]=t;
	red[0][1]=t2;

	int a[3];
	for(i=0;i<3;i++) a[i]=green[i][2];
	for(i=0;i<3;i++) green[i][2]=yellow[i][2];
	for(i=0;i<3;i++) yellow[i][2]=blue[2-i][0];
	for(i=0;i<3;i++) blue[2-i][0]=dark[i][2];
	for(i=0;i<3;i++) dark[i][2]=a[i];
	if(!pass) {
		if(obj.ch[1])
			obj.show(); 
		// cout<<"R ";
		push_spin('R', false);
	}
}
void _right()
{
	if(!_pass) {
		if(obj.ch[1])
			obj.show(); 
		// cout<<"R' ";
		push_spin('R', true);
	}
	pass=true;
	right();
	right();
	right();
	pass=false;
}

void left()
{
	int i;
	int t=orange[0][0];
	int t2=orange[1][0];
	orange[0][0]=orange[2][0];
	orange[1][0]=orange[2][1];
	orange[2][0]=orange[2][2];
	orange[2][1]=orange[1][2];
	orange[2][2]=orange[0][2];
	orange[1][2]=orange[0][1];
	orange[0][2]=t;
	orange[0][1]=t2;

	int a[3];

	for(i=0;i<3;i++) a[i]=green[i][0];
	for(i=0;i<3;i++) green[i][0]=dark[i][0];
	for(i=0;i<3;i++) dark[i][0]=blue[2-i][2];
	for(i=0;i<3;i++) blue[2-i][2]=yellow[i][0];
	for(i=0;i<3;i++) yellow[i][0]=a[i];
	if(!pass) {
		if(obj.ch[1])
			obj.show(); 
		//cout<<"L ";
		push_spin('L', false);
	}
}
void _left()
{
	if(!_pass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"L' ";
		push_spin('L', true);
	}
	pass=true;
	left();
	left();
	left();
	pass=false;
}

void up()
{
	int i;
	int t=dark[0][0];
	int t2=dark[1][0];
	dark[0][0]=dark[2][0];
	dark[1][0]=dark[2][1];
	dark[2][0]=dark[2][2];
	dark[2][1]=dark[1][2];
	dark[2][2]=dark[0][2];
	dark[1][2]=dark[0][1];
	dark[0][2]=t;
	dark[0][1]=t2;

	int a[3];

	for(i=0;i<3;i++) a[i]=green[0][i];
	for(i=0;i<3;i++) green[0][i]=red[0][i];
	for(i=0;i<3;i++) red[0][i]=blue[0][i];
	for(i=0;i<3;i++) blue[0][i]=orange[0][i];
	for(i=0;i<3;i++) orange[0][i]=a[i];
	if(!pass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"U ";
		push_spin('U', false);

	}

}
void _up()
{
	if(!_pass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"U' ";
		push_spin('U', true);

	}
	pass=true;
	up();
	up();
	up();
	pass=false;
}

void bot()
{
	int i;
	int t=yellow[0][0];
	int t2=yellow[1][0];
	yellow[0][0]=yellow[2][0];
	yellow[1][0]=yellow[2][1];
	yellow[2][0]=yellow[2][2];
	yellow[2][1]=yellow[1][2];
	yellow[2][2]=yellow[0][2];
	yellow[1][2]=yellow[0][1];
	yellow[0][2]=t;
	yellow[0][1]=t2;

	int a[3];

	for(i=0;i<3;i++) a[i]=green[2][i];
	for(i=0;i<3;i++) green[2][i]=orange[2][i];
	for(i=0;i<3;i++) orange[2][i]=blue[2][i];
	for(i=0;i<3;i++) blue[2][i]=red[2][i];
	for(i=0;i<3;i++) red[2][i]=a[i];
	if(!pass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"D ";
		push_spin('D', false);

	}
}
void _bot()
{
	if(!bpass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"D' ";
		push_spin('D', true);

	}
	pass=true;
	bot();
	bot();
	bot();
	pass=false;
}

void front()
{
	int i;
	int t=green[0][0];
	int t2=green[1][0];
	green[0][0]=green[2][0];
	green[1][0]=green[2][1];
	green[2][0]=green[2][2];
	green[2][1]=green[1][2];
	green[2][2]=green[0][2];
	green[1][2]=green[0][1];
	green[0][2]=t;
	green[0][1]=t2;

	int a[3];

	for(i=0;i<3;i++) a[i]=dark[2][i];
	for(i=0;i<3;i++) dark[2][i]=orange[2-i][2];
	for(i=0;i<3;i++) orange[2-i][2]=yellow[0][2-i];
	for(i=0;i<3;i++) yellow[0][2-i]=red[i][0];
	for(i=0;i<3;i++) red[i][0]=a[i];
	if(!pass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"F ";
		push_spin('F', false);
	}
}
void _front()
{
	if(!_pass) {
		if(obj.ch[1])
			obj.show();
		// cout<<"F' ";
		push_spin('F', true);
	}
	pass=true;
	front();
	front();
	front();
	pass=false;
}

void axis_u()
{
	if(!_pass) obj.add(green[1][1], red[1][1]);
	pass=true;
	bpass=true;
	up();
	mid_u();
	_bot();
	pass=false;
	bpass=false;
}

void _axis_u()
{
	obj.add(green[1][1], orange[1][1]);
	_pass=true;
	axis_u();
	axis_u();
	axis_u();
	_pass=false;
}

void mid_u()
{
	int i;
	int a[3];

	for(i=0;i<3;i++) a[i]=green[1][i];
	for(i=0;i<3;i++) green[1][i]=red[1][i];
	for(i=0;i<3;i++) red[1][i]=blue[1][i];
	for(i=0;i<3;i++) blue[1][i]=orange[1][i];
	for(i=0;i<3;i++) orange[1][i]=a[i];
}