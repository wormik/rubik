#include <iostream>
#include <GL/glut.h>
#include "rubik.h"
#include <vector>
using namespace std;

extern char green[3][3];
extern char red[3][3];
extern char orange[3][3];
extern char blue[3][3];
extern char yellow[3][3];
extern char dark[3][3];
extern vector<char> spins;

char toggle_case(char c)
{
	if (islower(c))
		return toupper(c);
	return tolower(c);
}

void print_spins()
{
	cout << endl << "Print spins:" << endl;
	for (auto c : spins) {
		if (islower(c))
			cout << (char)toupper(c) << "' ";
		else
			cout << c << " ";
	}
	cout << endl;
	cout << "Total moves: " << spins.size() << endl;
	spins.clear();
}

void refactor_spins()
{
	auto j = spins.begin();
	for (int i = 0; spins.size() >= 4 && i < spins.size() - 3; i++, j++) {
		if (spins[i] == spins[i + 1]
			&& spins[i + 1] == spins[i + 2]
			&& spins[i + 2] == spins[i + 3]) {
			spins.erase(j, j + 4);
		}
	}
	j = spins.begin();
	for (int i = 0; spins.size() >= 3 && i < spins.size() - 2; i++, j++) {
		if (spins[i] == spins[i + 1]
			&& spins[i + 1] == spins[i + 2]) {
			auto c = toggle_case(spins[i]);
			auto k = spins.erase(j, j + 3);
			spins.insert(k, c);
		}
	}
	j = spins.begin();
	for (int i = 0; spins.size() >= 2 && i < spins.size() - 1; i++, j++) {
		if (tolower(spins[i]) == tolower(spins[i + 1])
			&& (islower(spins[i]) &&  isupper(spins[i + 1])
				|| islower(spins[i + 1]) &&  isupper(spins[i]))) {
			spins.erase(j, j + 2);
		}
	}
}
void draw()
{
	glClear(GL_COLOR_BUFFER_BIT);
	drawDark();
	drawGreen();
	drawOrange();
	drawYellow();
	drawRed();
	drawBlue();
	glFlush();
}

void init() {
	glClearColor(219.0/255.0, 112.0/255.0, 147.0/255.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

void colorize(char col[3][3],int i,int j)
{
	switch(col[i][j]) {
	case 'g':
		glColor3f(0.0,1.0,0.0);
		break;
	case 'r':
		glColor3f(1.0,0.0,0.0);
		break;
	case 'o':
		glColor3f(1.0,140.0/255.0,0.0);
		break;
	case 'b':
		glColor3f(0.0,0.0,1.0);
		break;
	case 'y':
		glColor3f(1.0,1.0,0.0);
		break;
	case 'd':
		glColor3f(0.0,0.0,0.0);
		break;
	default:
		glColor3f(1.0,20.0/255.0,147.0/255.0);
		break;			
	}
} 

void navigate(unsigned char key, int x, int y)
{
	switch(key) {
	case 'a': 
		standard();
		glutPostRedisplay();
		break;
	case 'd':
		shuffle();
		glutPostRedisplay();
		break;
	case '0':
		exit(0);
		break;
	case '1':
		_1_cross();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case '2':
		_2_1floor();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case '3':
		_3_2floor();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case '4':
		_4_top_cross();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case '5':
		_5_3floor_edges();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case '6':
		_6_3floor_corners();
		cout<<"\n\n";
		while(green[1][1]!='g')axis_u();
		glutPostRedisplay();
		break;
	case '7':
		_7_final();
	   	refactor_spins();
		print_spins();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case 's':
		_1_cross();
		_2_1floor();
		_3_2floor();
		_4_top_cross();
		_5_3floor_edges();
		_6_3floor_corners();
		while(green[1][1]!='g')axis_u();
		_7_final();
	   	refactor_spins();
		print_spins();
		glutPostRedisplay();
		cout<<"\n\n";
		break;
	case 'c':
		system("clear");
		break;
	case 'm':
		system("clear");
		menu();
		break;
	}
}